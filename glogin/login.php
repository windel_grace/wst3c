<?php
		require_once "config.php";
		$loginURL = $gClient->createAuthUrl();
?>

		<?php 
		require_once "fconfig.php";
		if (isset($accessToken)){
		if (!isset($_SESSION['facebook_access_token'])){
		$_SESSION['facebook_access_token']=(string)$accessToken;
		$oAuth2Client=$fb->getOAuth2Client();
		$longLivedAccessToken=$oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token']=(string)$longLivedAccessToken;
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
}
		else{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
}

		try{
		$fb_response=$fb->get('/me?fields=name,first_name,last_name,email');
		$fb_response_pictur=fb->get('/mepicture?redirect=false&height=200');
		$fb_user=$fb_response->getGraphUser();
		$picture=$fb_response_picture->getGraphUser();
		$_SESSION['fb_user_id']=$fbuser->getProperty('id');
		$_SESSION['fb_user_name']=$fbuser->getProperty('name');
		$_SESSION['fb_user_email']=$fbuser->getProperty('email');
		$_SESSION['fb_user_pic']=$picture['url'];
}
		catch(Facebook\Exceptions\FacebookResponseException $e){
		echo'Facebook API Error: ' . $e->getMessage;
		session_destroy();
		header("Location:login.php");
		exit;
}
		catch(Facebook\Exceptions\FacebookResponseException $e){
		echo'Facebook SDK Error: ' . $e->getMessage;
}
}
		else{
		$fb_login_url=$fb_helper->getLoginUrl(FB_BASE_URL);
}
?>

<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta charset="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Log in FB/Google</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<script>	
</script>
</head>
<body>
		<br>
		<div class="date" style="float: right; margin-right: 200px;">
		<?php
		$date = date('M/d/y');
		echo ' ', $date;
?>
		<br>
		<?php
		date_default_timezone_set("Singapore");
		$time = date('h:i:s a');
		echo '', $time;
?>
</div>

		<div class="name" style="float: left; margin-left: 200px;">
		Name: Windel Grace F. Rodillas
		<br>
		Year & Section: BSIT-3C
		<br>
</div>
<br><br>
		<div class="container">
		<div class="row justify-content-center">
		<div align="center">
		<form>
		<img src="fb.png" width="40px" height="40px">
		<input type="button"  onclick="window.location = '<?php echo $fb_login_url ?>';" value="Login with facebook" class="btn btn-primary">
		&emsp;&emsp;
		<img src="google.png" width="50px" height="35px">
		<input type="button" onclick="window.location = '<?php echo $loginURL ?>';"  value="Sign in with google" class="btn btn-danger">
</form>
</div>	
</div>
</div>
</body>
</html>