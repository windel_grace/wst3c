<?php
		session_start();
		if (!isset($_SESSION['access_token'])){
		header('Location:login.php');
		exit();
}

?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta charset="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Log in FB/Google</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<br>
		<div class="date" style="float: right; margin-right: 200px;">
		<?php
		$date = date('M/d/y');
		echo ' ', $date;
		?>
		<br>
		<?php
		date_default_timezone_set("Singapore");
		$time = date('h:i:s a');
		echo '', $time;
?>
</div>
		<div class="name" style="float: left; margin-left: 200px;">
		Name: <?php echo $_SESSION['givenName']?> <?php echo $_SESSION['familyName']?>
		<br>
		Year & Section: BSIT3C
		<br>
</div>
<br><br><br>
		<div class="container">
		<div class="row justify-content-center">
		<div class="col-md-9" align="center">
		<h3>WELCOME!</h3>
		<h4>Successfully Sign In to Google!<h4/><br>
		<form action="logout.php">
		<input class="btn btn-success" type="submit" name="logout" value="Log out">

</form>
</div>	
</div>
</div>
</body>
</html>