@extends('master')

@section('title')
    <title>Templates</title>
@endsection

@section('content')

<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="topnav__link active">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="topnav__link">Admins</a>

    </li>
    <li class="topnav__item">
      <a href="/logout"><i class="fa fa-sign-out fa-2x text-danger"></i></a>
      
    </li>
    
  </ul>
</header>
<marquee behavior="" direction=""><span id='ct6' class="p-1" style="background-color: #FFBF00;"></span></marquee>



<div class="card mb-3 col-sm-6 mx-auto mt-4">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  @if(Session::has('success-template'))
    <div class="alert alert-success">
        {{ Session::get('success-template') }}
        @php
            Session::forget('success-template');
        @endphp
    </div>
  @endif


  

  <div class="card-body">
    <div class = "container col-sm-12">
      <form action = "" method = "post" enctype="multipart/form-data">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <div class="form-row">
   
          
          <div class="container mb-3">
            <div class="row">
              <table>
                <tbody>
                  <tr>
                    <td class="text-center">
                      <img src="assets/template1.png" alt="" style="width: 200px;">
                    </td>
                    <td class="text-center">
                      <img src="assets/template2.png" alt="" style="width: 200px;">
                    </td>
                  </tr>
                  <tr class="text-center fw-bold">
                    <td>
                      Template #1
                    </td>
                    <td>
                      Template #2
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="input-group mb-3">
            <span class="input-group-text text-white" style="background-color: #082b54;">Background Template</span>
            <select class="form-select" aria-label="Default select example" name="template">
              <option value="1">Template #1</option>
              <option value="2">Template #2</option>
            </select>
          </div>  
            <div class="input-group">
             
              <span  class="input-group-text text-white" style="background-color: #082b54;">Seminar</span>
              <select class="form-select" aria-label="#note" name="seminar">
                @foreach ($seminars as $seminar)
                <option value="{{$seminar->title}}">{{$seminar->title}}</option>
                @endforeach
              </select>
              <span aria-describedby="emailHelp"></span>

             
              
            </div>  
            <div id="emailHelp" class="form-text">Only one template per seminar</div>
            <div class="input-group my-3">
                <span class="input-group-text text-white" style="background-color: #082b54;">Contents</span>
                <textarea type="text" name="contents" value ="<?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?>" class="form-control"><?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?></textarea>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text text-white" style="background-color: #082b54;">Logo</span>
                <input type="file" name="logo" class="form-control" required>
            </div>

            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Signatory Name 1</span>
              <input type="text" name="signame1" class="form-control" value="<?php if (isset($_POST['signame1'])) {echo $_POST['signame1'];}?>">
              <span class="input-group-text text-white" style="background-color: #082b54;">Position</span>
              <input type="text" name="pos1" class="form-control"  value="<?php if (isset($_POST['pos1'])) {echo $_POST['pos1'];}?>">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">E-signature 1</span>
              <input type="file" name="esig1" class="form-control" value="<?php if (isset($_POST['esig1'])) {echo $_POST['esig1'];}?>">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Signatory Name 2</span>
              <input type="text" name="signame2" class="form-control" value="<?php if (isset($_POST['signame2'])) {echo $_POST['signame2'];}?>">
              <span class="input-group-text text-white" style="background-color: #082b54;">Position</span>
              <input type="text" name="pos2" class="form-control" value="<?php if (isset($_POST['pos2'])) {echo $_POST['pos2'];}?>">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">E-signature 2</span>
              <input type="file" name="esig2" class="form-control" value="<?php if (isset($_POST['esig2'])) {echo $_POST['esig2'];}?>">
            </div>
            
            <center>
              <button type="submit" name="preview" class="btn btn-lg btn-block mx-auto" style="background-color: #FFBF00;">Preview</button>
            </center>
        </div>
    </form>
	</div>
  </div>
</div>


<?php 
      if (isset($_POST['preview'])) {


        if($_POST['template'] == "1"){

          $seminar = $_POST['seminar'];
        // $title = $_POST['title'];
        // $title_len = strlen($_POST['title']);
        $contents= $_POST['contents'];
        // $url = $_POST['url'];
        $signame1 = $_POST['signame1'];
        $signame2 = $_POST['signame2'];
        $pos1 = $_POST['pos1'];
        $pos2 = $_POST['pos2'];
        $image_esig1 = $_FILES['esig1']['name'];
 
        $image_type = $_FILES['esig1']['type'];
        $image_size = $_FILES['esig1']['size'];
        $image_tmp_name= $_FILES['esig1']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig1");
 
 
 
        $image_esig2 = $_FILES['esig2']['name'];
        $image_type = $_FILES['esig2']['type'];
        $image_size = $_FILES['esig2']['size'];
        $image_tmp_name= $_FILES['esig2']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig2");
 
 
        $image_name = $_FILES['logo']['name'];
        $image_type = $_FILES['logo']['type'];
        $image_size = $_FILES['logo']['size'];
        $image_tmp_name= $_FILES['logo']['tmp_name'];
        move_uploaded_file($image_tmp_name,"logos/"."$image_name");
 
        if ($contents) {
          $font_size_content = 10;
        }
 
        if ($contents == "") {
          echo 
          "
          <div class='mx-auto alert alert-danger col-sm-6 text-center' role='alert'>
              Ensure you fill all the fields!
          </div>
          ";
        }else{
          echo 
          "
          <div class='mx-auto alert alert-success col-sm-6 text-center' role='alert' text-center>
              Here's the preview.
          </div>
          ";
 
          //designed certificate picture
          $image = "assets/template1.png";
 
          $createimage = imagecreatefrompng($image);
 
          //this is going to be created once the generate button is clicked
          $output = "previews/"."newpreview.png";
 
          //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
          $white = imagecolorallocate($createimage, 205, 245, 255);
          $black = imagecolorallocate($createimage, 0, 0, 0);
 
          //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
          $rotation = 0;
 
          //we then set the x and y axis to fix the position of our text name
          $origin_x = 200;
          $origin_y=250;
 
 
          //we then set the x and y axis to fix the position of our text occupation
          $origin1_x = 185;
          $origin1_y=300;
 
          //we then set the differnet size range based on the length of the text which we have declared when we called values from the form
          // if($title_len<=7){
          //   $font_size = 25;
          //   $origin_x = 190;
          // }
          // elseif($title_len<=12){
          //   $font_size = 30;
          // }
          // elseif($title_len<=15){
          //   $font_size = 26;
          // }
          // elseif($title_len<=20){
          //    $font_size = 18;
          // }
          // elseif($title_len<=22){
          //   $font_size = 15;
          // }
          // elseif($title_len<=33){
          //   $font_size=11;
          // }
          // else {
          //   $font_size =10;
          // }
 
          //$certificate_text = $title;
 
          //font directory for name
          $drFont = "assets/Allura-Regular.TTF";
 
          // font directory for occupation name
          $drFont1 = "assets/Poppins-SemiBold.TTF";
 
          // font directory for positions
          $posFont = "assets/Nunito-VariableFont_wght.TTF";
 
          //function to display name on certificate picture
          //$text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);
 
          //function to display occupation name on certificate picture
          $text2 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $contents);
 
          $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, 30, $black, $drFont1, $seminar);
 
          $text4 = imagettftext($createimage, $font_size_content, $rotation, 250, 390, $black, $drFont1, $signame1);
 
          $text5 = imagettftext($createimage, $font_size_content, $rotation, 470, 390, $black, $drFont1, $signame2);
 
          $text6 = imagettftext($createimage, 9, $rotation, 270, 410, $black, $posFont, $pos1);
 
          $text7 = imagettftext($createimage, 9, $rotation, 490, 410, $black, $posFont, $pos2);
 
 
 
          $esig1=imagecreatefrompng("signatures/"."$image_esig1");
          imagecopy($createimage,$esig1, 250, 340, 0, 0, 100, 50);
 
 
 
          $esig2=imagecreatefrompng("signatures/"."$image_esig2");
          imagecopy($createimage,$esig2, 460, 340, 0, 0, 100, 50);
 
          // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);
 
          $str=imagecreatefrompng("logos/"."$image_name");
          imagecopy($createimage,$str, 20, 30 , 0, 0, 100, 100);
 
          // $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$url."&choe=UTF-8"));
              // imagecopyresampled($createimage, $QR, 15, 352 , 0, 0, 110, 110, 100,100);
 
          imagepng($createimage,$output,3);
 

 ?>


        <!-- this displays the image below -->
        <div class = "container col-sm-6">
        	<img class="mx-auto d-block" src="<?php echo $output; ?>" >
        </div>
        
        <center>
          <div class="mt-4">
            <a href="templates/save/<?php echo $seminar."/".$_POST['template']; ?>" class="btn btn-success">Add Design</a>
          </div>
        </center>

        <!-- this provides a download button -->
        
        <br><br>
<?php 
        }

        }else{
          $seminar = $_POST['seminar'];
        $seminar_len = strlen($_POST['seminar']);
        // $title = $_POST['title'];
        // $title_len = strlen($_POST['title']);
        $contents= $_POST['contents'];
        // $url = $_POST['url'];
        $signame1 = $_POST['signame1'];
        $signame2 = $_POST['signame2'];
        $pos1 = $_POST['pos1'];
        $pos2 = $_POST['pos2'];
        $image_esig1 = $_FILES['esig1']['name'];
 
        $image_type = $_FILES['esig1']['type'];
        $image_size = $_FILES['esig1']['size'];
        $image_tmp_name= $_FILES['esig1']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig1");
 
 
 
        $image_esig2 = $_FILES['esig2']['name'];
        $image_type = $_FILES['esig2']['type'];
        $image_size = $_FILES['esig2']['size'];
        $image_tmp_name= $_FILES['esig2']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig2");
 
 
        $image_name = $_FILES['logo']['name'];
        $image_type = $_FILES['logo']['type'];
        $image_size = $_FILES['logo']['size'];
        $image_tmp_name= $_FILES['logo']['tmp_name'];
        move_uploaded_file($image_tmp_name,"logos/"."$image_name");
 
        if ($contents) {
          $font_size_content = 10;
        }
 
        if ($contents == "") {
          echo 
          "
          <div class='mx-auto alert alert-danger col-sm-6 text-center' role='alert'>
              Ensure you fill all the fields!
          </div>
          ";
        }else{
          echo 
          "
          <div class='mx-auto alert alert-success col-sm-6 text-center' role='alert' text-center>
              Here's the preview.
          </div>
          ";
 
          //designed certificate picture
          $image = "assets/template2.png";
 
          $createimage = imagecreatefrompng($image);
 
          //this is going to be created once the generate button is clicked
          $output = "previews/"."newpreview.png";
 
          //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
          $white = imagecolorallocate($createimage, 205, 245, 255);
          $black = imagecolorallocate($createimage, 0, 0, 0);
 
          //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
          $rotation = 0;
 
          //we then set the x and y axis to fix the position of our text name
          $origin_x = 200;
          $origin_y=250;
 
 
          //we then set the x and y axis to fix the position of our text occupation
          $origin1_x = 185;
          $origin1_y=300;
 
          $sem_x = 160;
          $sem_y = 80;
 
          if($seminar_len<=50){
            $sem_x = 210;
          }
 
          if($seminar_len<=10){
            $sem_x = 320;
          }
 
          //$certificate_text = $title;
 
          //font directory for name
          $drFont = "assets/Allura-Regular.TTF";
 
          // font directory for occupation name
          $drFont1 = "assets/Poppins-SemiBold.TTF";
 
          // font directory for positions
          $posFont = "assets/Nunito-VariableFont_wght.TTF";
 
          //function to display name on certificate picture
          //$text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);
 
          //function to display occupation name on certificate picture
          $text2 = imagettftext($createimage, $font_size_content, $rotation, 120, 265, $black, $drFont1, $contents);
 
          $text3 = imagettftext($createimage, $font_size_content, $rotation, $sem_x, $sem_y, $black, $drFont1, $seminar);
 
          $text4 = imagettftext($createimage, $font_size_content, $rotation, 180, 370, $black, $drFont1, $signame1);
 
          $text5 = imagettftext($createimage, $font_size_content, $rotation, 400, 370, $black, $drFont1, $signame2);
 
          $text6 = imagettftext($createimage, 9, $rotation, 200, 385, $black, $posFont, $pos1);
 
          $text7 = imagettftext($createimage, 9, $rotation, 420, 385, $black, $posFont, $pos2);
 
 
 
          $esig1=imagecreatefrompng("signatures/"."$image_esig1");
          imagecopy($createimage,$esig1, 175, 320, 0, 0, 100, 50);
 
 
 
          $esig2=imagecreatefrompng("signatures/"."$image_esig2");
          imagecopy($createimage,$esig2, 400, 320, 0, 0, 100, 50);
 
          // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);
 
          $str=imagecreatefrompng("logos/"."$image_name");
          imagecopy($createimage,$str, 60, 30 , 0, 0, 100, 100);
 
          // $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$url."&choe=UTF-8"));
              // imagecopyresampled($createimage, $QR, 15, 352 , 0, 0, 110, 110, 100,100);
 
          imagepng($createimage,$output,3);

 ?>


        <!-- this displays the image below -->
        <div class = "container col-sm-6">
        	<img class="mx-auto d-block" src="<?php echo $output; ?>" >
        </div>
        
        <center>
          <div class="mt-4">
            <a href="templates/save/<?php echo $seminar."/".$_POST['template']; ?>" class="btn btn-success">Add Design</a>
          </div>
        </center>

        <!-- this provides a download button -->
        
        <br><br>
<?php 
        }

        }


      }

     ?>



<script>
  $(function(){
        $("#templatesTable").DataTable();
    });
</script>


@push('javascript-external')
<script src="{{asset('vendor/tinymce5/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendor/tinymce5/tinymce.min.js')}}"></script>
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush

@push('javascript-internal')
<script>
      $(document).ready(function() {
         //Event: delete
         $("form[role='alert']").submit(function(event) {
            event.preventDefault();
            Swal.fire({
            title: $(this).attr('alert-title'),
            text:  $(this).attr('alert-text'),
            icon: 'warning',
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: $(this).attr('alert-btn-cancel'),
            reverseButtons: true,
            confirmButtonText: $(this).attr('alert-btn-yes'),
         }).then((result) => {
            if (result.isConfirmed) {
               //process ng deleting 
              event.target.submit();
             
            }
         });

         });
        //Texteditor content
      $("#input_template_content").tinymce({
      relative_urls: false,
      language: "en",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table directionality",
        "emoticons template paste textpattern",
      ],
      toolbar1: "fullscreen preview",
      toolbar2:
        "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        });

      });
      
  </script>
@endpush

<div class="container">
  <div class="row my-5">
    <div class="col-sm-12 mx-auto">
      <table class="table w-100 mx-auto" id="templatesTable">
        <thead class="bg-dark text-white">
          <tr>
            <th>ID</th>
            <th>Seminar Title</th>
            <th>Template</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($templates as $template)
          <tr>
            <td>{{$template->id}}</td>
            
            <td style="width: 300px;">
              <a target="_blank" href="{{$template->img_path}}"><img class="img-fluid w-100" src="{{$template->img_path}}" alt="{{$template->seminar_name}}"></a>
              
            </td>
            <td>
              {{$template->seminar_name}}
              
            </td>
            <td>
              <form action="/templates/delete/{{$template->id}}" role="alert" method="GET" 
                alert-title="{{trans('template.alert.delete.title')}}" alert-text="{{trans('template.alert.delete.message.confirm',['title' => $template->title])}}"
                alert-btn-cancel="{{trans('template.button.cancel.value')}}" alert-btn-yes="{{trans('template.button.delete.value')}}" >
              <div class="text-center">
              <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                  </button>
              </div>
            </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@include('sweetalert::alert')

@stack('javascript-external')
@stack('javascript-internal')

@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush


@endsection