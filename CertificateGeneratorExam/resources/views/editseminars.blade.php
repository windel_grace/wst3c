@extends('master')

@section('title')
    <title>Seminars</title>
@endsection

@section('content')


<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="{{url('seminars')}}" class="topnav__link active">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('templates')}}" class="topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('generator')}}" class="topnav__link">Certificate Generator</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('certs')}}" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('logout')}}" class="topnav__link">Logout</a>
    </li>
  </ul>
</header>

<div class="container">
  @if(Session::has('success-seminar'))
  <div class="alert alert-success">
      {{ Session::get('success-seminar') }}
      @php
          Session::forget('success-seminar');
      @endphp
  </div>
  @endif
</div>



<div class="card mb-3 col-sm-6 mx-auto mt-4">
  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif

  <div class="card-body">
    <div class = "container col-sm-12">
    @foreach ($seminars as $seminar)
		<form action="/seminars/edit/{{$seminar->id}}" method = "post" enctype="multipart/form-data">
	        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
	        <div class="form-row">
	            <div class="input-group mb-3">
	                <span class="input-group-text text-white" style="background-color: #082b54;">Title</span>
	                <input type="text" name="title" class="form-control" value="{{$seminar->title}}">
	            </div>
	            <div class="input-group">
                <span class="input-group-text text-white" style="background-color: #082b54;">Date</span>
                <input type="date" class="form-control" name="sdate" value="{{$seminar->sdate}}"> 
                <span class="p-2 fw-bold">TO</span>
                <input type="date" class="form-control" name="edate" value="{{$seminar->edate}}">
                <span aria-describedby="dateHelp"></span>
                
              </div>
              <div id="dateHelp" class="form-text mb-3">The date must be the day after tomorrow</div>

              <div class="input-group mb-3">
                <span  class="input-group-text text-white" style="background-color: #082b54;">Status</span>
                <select class="form-select" aria-label="Default select example" name="status">
                  <option value="1"
                  @if ($seminar->status == "1")
                      selected
                  @endif
                  >Active</option>
                  <option value="0"
                  @if ($seminar->status == "0")
                      selected
                  @endif
                  >Inactive</option>
                </select>
              </div> 

	            <div class="input-group mb-3">
	                <span class="input-group-text text-white" style="background-color: #082b54;">Venue</span>
	                <input type="text" name="venue" class="form-control"  value="{{$seminar->venue}}">
	            </div>


	            <center>
	            	<button type="submit" class="btn btn-primary btn-block mx-auto">Update Seminar</button>
	            </center>
	        </div>
	    </form>
    @endforeach
	</div>
  </div>
</div>

@endsection