@extends('master')

@section('title')
    <title>Admin Accounts</title>
@endsection

@section('content')



<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="topnav__link active">Admins</a>

    </li>
    <li class="topnav__item">
      <a href="/logout"><i class="fa fa-sign-out fa-2x text-danger"></i></a>
      
    </li>
    
  </ul>
</header>
<marquee behavior="" direction=""><span id='ct6' class="p-1" style="background-color: #FFBF00;"></span></marquee>


<div class="card mb-3 col-sm-6 mx-auto mt-4">
  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
  
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  @if(Session::has('success-seminar'))
    <div class="alert alert-success">
        {{ Session::get('success-seminar') }}
        @php
            Session::forget('success-seminar');
        @endphp
    </div>
  @endif
  
  
  
  <div class="card-body">
    <div class = "container col-sm-12">
    <form action="/admins" method = "post" enctype="multipart/form-data">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
          <div class="form-row">
           
  
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Email</span>
              <input type="email" name="email" class="form-control">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Username</span>
              <input type="text" name="username" class="form-control">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Password</span>
              <input type="password" name="password" class="form-control">
            </div>

            <center>
              <button type="submit" class="btn mx-auto" style="background-color: #FFBF00">Add Admin</button>
            </center>
          </div>
      </form>
  </div>
  </div>
  </div>




<script>
$(function(){
      $("#adminsTable").DataTable();
  });
</script>



<div class="container">
<div class="row my-5">
  <div class="col-sm-12 mx-auto">
    <table class="table w-100 mx-auto" id="adminsTable">
      <thead class="bg-dark text-white">
        <tr>
          <th>ID</th>
          <th>Email</th>
          <th>Username</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($admins as $admin)
        <tr>
          <td>{{$admin->admin_id}}</td>
          <td>{{$admin->email}}</td>
          <td>{{$admin->username}}</td>
          <td class="text-center">

            <a href="/admins/delete/{{$admin->admin_id}}" class="btn btn-danger"><i class=" fas fa-trash"></i></a>
              {{-- <form action="/admins/delete/{{$admin->admin_id}}" role="alert" method="GET" 
              alert-title="{{trans('admins.alert.delete.title')}}" alert-text="{{trans('admins.alert.delete.message.confirm',['title' => $admin->email])}}"
              alert-btn-cancel="{{trans('admins.button.cancel.value')}}" alert-btn-yes="{{trans('admins.button.delete.value')}}">
                <div class="text-center">
                  @csrf
                  <!-- @method('DELETE') -->
                  <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                  </button>
                </div>
              </form> --}}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>


@endsection


