@extends('master')

@section('title')
    <title>Verification</title>
@endsection

@section('content')

<style type="text/css">
  @import url('https://fonts.googleapis.com/css?family=Raleway:400,700');
  * {
    font-family: 'Raleway', monospace;
    letter-spacing: 1px;
    word-spacing: 1px;
    color: #040404;
    background-color: transparent;
    box-sizing: border-box;
  }

  header {
    text-align: center;
    word-spacing: 20px;
    letter-spacing: 5px;
    padding: 1px;
    margin: auto;
    border-bottom: dotted #748494 3pt;
  }

  .title, #parabola {
    font-size: 30pt;
    font-weight: 1;
    color: #F42C1B;
  }

  #parabola {
    color: #04747C;
    font-size: 50pt;
    background-color: transparent;
  }
</style>


<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">

</header>



<header>
  <h1 class="title"><span id="parabola">Certificate</span><br>Verification</h1>
  <p>To verify your certificate, please fill in your certificate code below.</p>
  <div class ="row">
    <div class="container col-sm-6">
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @if(Session::has('success-generator'))
        <div class="alert alert-success">
            {{ Session::get('success-generator') }}
            @php
                Session::forget('success-generator');
            @endphp
        </div>
      @endif
    <form action = "/" method = "post" enctype="multipart/form-data">
      <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
      <div class="form-row">
          <div class="col-sm-4 input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Generated QR Code</span>
              <input type="text" name="token" value ="" class="form-control">
              <button class="btn" type="submit" style="margin-left: 5px; width: 150px; background-color:#FFBF00;">Verify</button>
          </div>
      </div>
    </form>
    </div>
  </div>
  
</header>
@if (count($details)>0)
    <div class="container mx-auto mt-4">
      <div class="card">
      <h5 class="card-header text-white" style="background-color: #082b54;"><b class=" text-white">Your certificate is valid.</b></h5>
      <div class="card-body">
        <h5 class="card-title"><b>Token:</b> {{$details['token']}}</h5>
        <h5 class="card-title mt-4"><b>Participant Profile</b></h5>
        <p class="card-text"><b>Name:</b> {{$details['name']}}</p>
        <p class="card-text"><b>Seminar:</b> {{$details['seminar']}}</p>
        <p class="card-text"><b>Venue:</b> {{$details['venue']}}</p>
        <p class="card-text"><b>Date:</b> {{$details['sdate']}}
        @if ($details['edate'] != $details['sdate'])
        to {{$details['edate']}}
        @endif
        
        </p>
      <div class="card-footer">
        <p>You can redownload your certificate by clicking the button below.</p>
        <a download="{{$details['img_path']}}" target="_blank" href="{{$details['img_path']}}"  class="btn" style="background-color:#FFBF00">Download Certificate</a>
      </div>
        
      </div>
    </div>
    </div>
    
@else
    <div class="container" style="height:300px;"></div>
@endif


@endsection



