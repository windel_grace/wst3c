<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use App\Models\Template;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class TemplatesController extends Controller
{
    //

    public function displayTemplate(Request $request){
        $templates = Template::all();


        $seminars = Seminar::where([
            ['status', '=', '1']
        ])->get();

        // $seminars = Seminar::all();

        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function previewTemplate(Request $request){
        $request->validate([
            'template' => 'required',
            'seminar' => 'required|unique:templates,seminar_name',
            'contents' => 'required',
            'logo' => 'required',
            'signame1' => 'required',
            'pos1' => 'required',
            'esig1' => 'required',
            'signame2' => 'required',
            'pos2' => 'required',
            'esig2' => 'required',
        ]);

        $templates = Template::all();
        $seminars = Seminar::where([
            ['status', '=', '1']
        ])->get();

        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function saveTemplate(Request $request, $seminar_name, $temp){
        //move previews/newpreview to templates folder

        $ldate = date('dH-i-s');

        $filename = "templatesFolder/seminar_".$ldate.".png";

        $template=0;
        if($temp == "2"){
            $template=1;
        }


       $insert = [
        "seminar_name" => $seminar_name,
        "img_path" => $filename,
        "template" => $template,
       ];




       Template::create($insert);
       File::move(public_path('previews/newpreview.png'), public_path($filename));

       Alert::success('Success Template', 'Template created Successfully');
       return redirect('templates');

    }

    public function deleteTemplate(Request $request, $id){

       $template = Template::where([
        'id' => $request->id
        ])->get();


        if(count($template)>0){

            foreach($template as $templates){
                $img = $templates->img_path;
            }

            unlink($img);
            Template::where("id", $request->id)->delete();
            Alert::success('Success Template', 'Template deleted Successfully');
            return redirect('templates');
        }else{
            Alert::error('Template Error', 'An error occurred while deleting the template');
            return redirect('templates');
        }

    }
}
