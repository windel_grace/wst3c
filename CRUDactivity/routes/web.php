<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\userInput;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//users
Route::get('login', [MainController::class, 'login'])->name('login');
Route::get('register', [MainController::class, 'register'])->name('register');
Route::post('save', [MainController::class, 'save'])->name('save');
Route::post('check', [MainController::class, 'check'])->name('check');

//appointment
Route::get('appointment', [MainController::class, 'appointment'])->name('appointment');
Route::post('saveapp', [MainController::class, 'saveapp'])->name('saveapp');
 
//admin
Route::get('admin', [MainController::class, 'adminLogin'])->name('adminLogin');
Route::post('checkadmin', [MainController::class, 'checkAdmin'])->name('checkAdmin');

//Route::get('appointment', function () {
   //return view('appointment');
//});

