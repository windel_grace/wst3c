<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class MainController extends Controller
{
    function saveapp(Request $request){
            
        $sched=DB::table('inputs')->where('date',$request->date)->where('time',$request->time)->first();
            if ($sched){
                return back()->with('fail', 'The schedule is already taken, Please reschedule');
            }else{
                $add=DB::table('inputs')->insert([
                    'date' => $request->date,
                    'time' => $request->time,
                ]);
                return back()->with('success', 'Successfully, Wait for your scheduled date, Thank you!');
            }
            
        $request->validate([
            'time'=>'required',
            'date'=>'required',
        ]);
            $input = new Input;
            $input->date=$request->date;
            $input->time=$request->time;
            $saveapp = $input->save();

            if ($saveapp){
            return back()->with('success','Successfully, Wait for your scheduled date, Thank you!');
        }else{
            return back()->with('fail','The Doctor is not available in this date and time');
        }


            }

            function appointment(){
                return view('appointment');
            }

            function adminLogin(){
                return view('admin');
            }

            function checkAdmin(Request $request){
                //validate inputs
        $request->validate([
            'username'=>'required',
            'password'=>'required|min:5|max:12'
        ]);


        $userInput = Admin::where('username','=', $request->username)->first();

        if(!$userInput){
            return back()->with('fail', 'We do not recognize this Username');
            }else{
                //check pass
                if(Hash::check($request->password, $userInput->password)){
                    $request->session()->put('LoogedUser', $userInput->id);
                    return redirect('appointment');

                }else{
                    return back()->with('fail', 'Incorrect Password');
                }
            }


            }


    function login(){
        return view('login');
    }

    function register(){
        return view ('register');
    }

    function save(Request $request){
        
        //validation
        $request->validate([
            'name'=>'required',
            'username'=>'required',
            'email'=>'required|email|unique:admins',
            'password'=>'required|min:5|max:12'
        ]);

        //inserting data into db. Model way

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $save = $admin->save();

        if ($save){
            return back()->with('success','New user has been successfuly added');
        }else{
            return back()->with('fail','Something went wrong, try again later');
        }
    }

    function check(Request $request){
        //validate inputs
        $request->validate([
            'username'=>'required',
            'password'=>'required|min:5|max:12'
        ]);


        $userInput = Admin::where('username','=', $request->username)->first();

        if(!$userInput){
            return back()->with('fail', 'We do not recognize this Username');
            }else{
                //check pass
                if(Hash::check($request->password, $userInput->password)){
                    $request->session()->put('LoogedUser', $userInput->id);
                    return redirect('appointment');

                }else{
                    return back()->with('fail', 'Incorrect Password');
                }
            }

            

            

            function appoint(){
                return view('appointment');
            }

            


}
}
    


