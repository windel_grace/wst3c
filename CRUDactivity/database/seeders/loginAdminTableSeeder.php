<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\loginAdmin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class loginAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        loginAdmin::create([
            'username'  =>  'admin',
            'password'  =>  Hash::make('admin123'),
        ]);
    }
}