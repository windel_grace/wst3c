<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Set Appointment</title>
    <style>
     .container h4{
     	text-align: center;
     	margin-top: 20px;
     }
      .btn{
      width: 100%;
      background: none;
      border: 2px solid #ECC4BA;
      padding: 5px;
      font-size: 18 px;
      cursor: pointer;
      margin: 12px 0;
      color: black;
    }
    </style>
</head>
    <body>

    	<div class="container">
    <div class="row">
        <div class="col-md-13">
            <div class="card">
                <div class="card-header">
                    <h4>Set your schedule here!
                        <a href="{{ route('login') }}" class="btn btn-danger float-end">Log out</a>
                    </h4>
                </div>
                <br><br><br>
        <div class="card-body">
                    <table class="table table-hover">
                    	<thead>
                    	
                    		<th>Date</th>
                    		<th>Time</th>
                    		<th>Book</th>
                    	</thead>

                    	<tbody>
                    		<form action="{{ route('saveapp') }}" method="post">
                    			
							@if(Session::get('success'))
							<div class="alert alert-success">{{Session::get('success')}}
							</div>
							@endif

							@if(Session::get('fail'))
							<div class="alert alert-danger">{{Session::get('fail')}}</div>
							@endif

							<div class="form-group">
                    		<td><input type="date" name="date" required></td>
                    		</div>
                    		<span class="text-danger">@error('date'){{$message}} @enderror</span>

                    		<td>
								{{csrf_field()}}
                    			<div class="form-group">
								<select name="time" required>
									<option>9:00 AM - 10:00 AM</option>
									<option>10:00 AM - 12:00 AM</option>
									<option>2:00 PM - 3:00 PM</option>
									<option>3:30 PM - 5:00 PM</option>
								</select>
								</div>
								<span class="text-danger">@error('time'){{$message}} @enderror</span>

                    		</td>
                    		
                    		<td><button type="submit" class="btn btn-block btn-success">Submit</button></td>
                    		</form>
                    	</tbody>
                    </table>
                </div>

            </div>
            
        </div>
        
    </div>
    
</div>
            
            
    </body>
</html>