-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 06:24 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_etr`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Beautiful days begin with a beautiful mindset. Hi, My name is Hazel Anne Mendoza Salazar. My friends and family call me Hazel. I was born on the 10th day of August in the year 2000 at the sacred heart hospital in Urdaneta city.', '2022-06-05 03:12:34', '2022-06-10 08:42:18'),
(4, 'I love dancing ballroom, joining beauty pageants, and playing baton stick. I love eating seafood and meat dishes. I am artistic and passionate in everything I do and a good listener too.', '2022-06-10 08:45:34', '2022-06-10 08:45:34'),
(5, 'Through my present hobby I can express my feelings and show up my passion and talent. I Love baking and designing beautiful cakes and pastries. I learned that positive mindset, determination, and skills from my previous instructor.', '2022-06-10 08:46:00', '2022-06-10 08:46:00'),
(6, 'Now my hobby turned into my present business which I named after my name \"Hazel\'s cakes and bakes\" I wake up each day with a smile, waiting for more orders which I am very grateful to my clients.', '2022-06-10 08:46:21', '2022-06-10 08:46:21'),
(7, 'I believed indeed that the beautiful days begin with a beautiful mindset.', '2022-06-10 08:46:26', '2022-06-10 08:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `address`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'San Hilario St. Puelay Villasis Pangasinan', '09668042482', 'hazelannesalazar@gmail.com', '2022-06-05 02:24:31', '2022-06-10 09:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `homes`
--

CREATE TABLE `homes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homes`
--

INSERT INTO `homes` (`id`, `text`, `created_at`, `updated_at`) VALUES
(3, 'Home Made. Fresh. Baked with Love.', '2022-06-10 07:35:18', '2022-06-16 04:56:55'),
(4, 'Life is short . Make it Sweet.', '2022-06-16 04:56:14', '2022-06-16 04:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(4, '2022_05_23_104333_create_users_table', 1),
(5, '2022_06_04_122718_create_products_table', 1),
(6, '2022_06_05_093301_create_contacts_table', 2),
(7, '2022_06_05_103801_create_aboutus_table', 3),
(8, '2022_06_05_103801_create_abouts_table', 4),
(9, '2022_06_05_111457_create_homes_table', 5),
(10, '2022_06_16_011941_create_messages_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `image`, `created_at`, `updated_at`) VALUES
(12, 'Birthday Cake', 'Price: 800php', '20220611131412.jpg', '2022-06-11 02:28:38', '2022-06-15 01:54:47'),
(13, 'Birthday Cake', 'Price: 1,200php', '20220611103748.jpg', '2022-06-11 02:37:48', '2022-06-15 02:07:33'),
(14, 'Birthday Cake', 'Price: 1,200php', '20220611131034.jpg', '2022-06-11 05:10:34', '2022-06-15 02:07:47'),
(15, 'Birthday Cake', 'Price: 1,000php', '20220611131615.jpg', '2022-06-11 05:16:15', '2022-06-15 02:07:18'),
(16, 'Birthday Cake', 'Price: 800php', '20220611131637.jpg', '2022-06-11 05:16:37', '2022-06-15 02:07:03'),
(17, 'Birthday Cake', 'Price: 900php', '20220611131652.jpg', '2022-06-11 05:16:52', '2022-06-15 02:06:44'),
(18, 'Birthday Cake', 'Price: 500php', '20220611132040.jpg', '2022-06-11 05:20:40', '2022-06-15 02:06:31'),
(19, 'Birthday Cake', 'Price: 1,300php', '20220611132138.jpg', '2022-06-11 05:21:38', '2022-06-15 02:06:17'),
(20, 'Kiddie Cake', 'Price: 1,000php', '20220611132210.jpg', '2022-06-11 05:22:10', '2022-06-15 02:06:01'),
(21, 'Kiddie Cake', 'Price: 1,800php', '20220611132223.jpg', '2022-06-11 05:22:23', '2022-06-15 02:05:33'),
(22, 'Kiddie Cake', 'Price: 1,600php', '20220611132241.jpg', '2022-06-11 05:22:41', '2022-06-15 02:05:15'),
(23, 'Kiddie Cake', 'Price: 1,700php', '20220611132309.jpg', '2022-06-11 05:23:09', '2022-06-15 02:05:00'),
(24, 'Number Cake', 'Price: 300php', '20220611132433.jpg', '2022-06-11 05:24:33', '2022-06-15 02:04:43'),
(25, 'Number Cake', 'Price: 300php', '20220611132447.jpg', '2022-06-11 05:24:47', '2022-06-15 02:04:31'),
(26, 'Number Cake', 'Price: 300php', '20220611132633.jpg', '2022-06-11 05:25:12', '2022-06-15 02:04:20'),
(27, 'Number Cake', 'Price: 250php', '20220611132533.jpg', '2022-06-11 05:25:33', '2022-06-15 02:04:08'),
(28, 'Bento Cake', 'Price: 300php', '20220611132708.jpg', '2022-06-11 05:27:08', '2022-06-15 02:03:50'),
(29, 'Bento Cake', 'Price: 300php', '20220611132854.jpg', '2022-06-11 05:27:32', '2022-06-15 02:03:39'),
(30, 'Bento Cake', 'Price: 300php', '20220611132751.jpg', '2022-06-11 05:27:51', '2022-06-15 02:03:28'),
(31, 'Bento Cake', 'Price: 300php', '20220611132813.jpg', '2022-06-11 05:28:13', '2022-06-15 02:03:16'),
(32, 'Two Tier Cake', 'Price: 1,900php', '20220611133047.jpg', '2022-06-11 05:30:47', '2022-06-15 02:02:58'),
(33, 'Two Tier Cake', 'Price: 1,900php', '20220611133133.jpg', '2022-06-11 05:31:33', '2022-06-15 02:02:41'),
(34, 'Cake w/ cupcakes', 'Price: 1,500php', '20220611133212.jpg', '2022-06-11 05:32:12', '2022-06-15 02:02:24'),
(35, 'Cake w/ cupcakes', 'Price: 1,800php', '20220611133234.jpg', '2022-06-11 05:32:34', '2022-06-15 02:02:09'),
(36, 'Birthday Cake', 'Price: 1,100php', '20220611135953.jpg', '2022-06-11 05:59:53', '2022-06-15 02:01:53'),
(37, 'Birthday Cake', 'Price: 1,100php', '20220611140023.jpg', '2022-06-11 06:00:23', '2022-06-15 02:01:39'),
(38, 'Birthday Cake', 'Price: 1,000php', '20220611140104.jpg', '2022-06-11 06:01:04', '2022-06-15 02:01:24'),
(39, 'Kiddie Cake', 'Price: 1,200php', '20220611140212.jpg', '2022-06-11 06:02:12', '2022-06-15 02:01:11'),
(40, 'Kiddie Cake', 'Price: 1,000php', '20220611140248.jpg', '2022-06-11 06:02:48', '2022-06-15 02:00:57'),
(41, 'Kiddie Cake', 'Price: 1,200php', '20220611140318.jpg', '2022-06-11 06:03:18', '2022-06-15 02:00:45'),
(42, 'Kiddie Cake', 'Price: 1,300php', '20220611140342.jpg', '2022-06-11 06:03:42', '2022-06-15 02:00:27'),
(43, 'Kiddie Cake', 'Price: 1,200php', '20220611140452.jpg', '2022-06-11 06:04:52', '2022-06-15 02:00:10'),
(44, 'Cupcakes', 'Price: 500php', '20220611142459.jpg', '2022-06-11 06:24:59', '2022-06-15 01:59:54'),
(45, 'Cupcakes', 'Price: 200php', '20220611142518.jpg', '2022-06-11 06:25:18', '2022-06-15 01:59:39'),
(46, 'Cupcakes', 'Price: 200php', '20220611142536.jpg', '2022-06-11 06:25:36', '2022-06-15 01:59:27'),
(47, 'Cupcakes', 'Price: 300php', '20220611142612.jpg', '2022-06-11 06:26:12', '2022-06-15 01:59:13'),
(48, 'Cupcakes', 'Price: 250php', '20220611142651.jpg', '2022-06-11 06:26:51', '2022-06-15 01:58:57'),
(49, 'Cupcakes', 'Price: 250php', '20220611142706.jpg', '2022-06-11 06:27:06', '2022-06-15 01:58:46'),
(50, 'Cupcakes', 'Price: 280php', '20220611142723.jpg', '2022-06-11 06:27:23', '2022-06-15 01:58:09'),
(51, 'Cupcakes', 'Price: 280php', '20220611142753.jpg', '2022-06-11 06:27:53', '2022-06-15 01:57:53'),
(52, 'Birthday Cake', 'Price: 1,3000php', '20220611145158.jpg', '2022-06-11 06:51:58', '2022-06-15 01:57:39'),
(53, 'Birthday Cake', 'Price: 1,200php', '20220611145212.jpg', '2022-06-11 06:52:12', '2022-06-15 01:57:11'),
(54, 'Birthday Cake', 'Price: 1000php', '20220611145227.jpg', '2022-06-11 06:52:27', '2022-06-15 01:56:54'),
(55, 'Birthday Cake', 'Price: 1,000php', '20220611145245.jpg', '2022-06-11 06:52:45', '2022-06-15 01:57:22'),
(56, 'Mother\'s Day Cake', 'Price: 300php', '20220615032822.jpg', '2022-06-14 19:28:22', '2022-06-15 01:56:25'),
(57, 'Mother\'s Day Cake', 'Price: 300php', '20220615032842.jpg', '2022-06-14 19:28:42', '2022-06-15 01:56:16'),
(58, 'Mother\'s Day Cake', 'Price: 300php', '20220615032859.jpg', '2022-06-14 19:28:59', '2022-06-15 01:56:05'),
(59, 'Mother\'s Day Cake', 'Price: 500php', '20220615032915.jpg', '2022-06-14 19:29:15', '2022-06-15 01:55:54'),
(60, 'Father\'s Day Cake', 'Price: 700php', '20220615033024.jpg', '2022-06-14 19:30:24', '2022-06-15 01:55:42'),
(61, 'Father\'s Day Cake', 'Price: 700php', '20220615033044.jpg', '2022-06-14 19:30:45', '2022-06-15 01:55:26'),
(62, 'Father\'s Day Cake', 'Price: 500php', '20220615033102.jpg', '2022-06-14 19:31:02', '2022-06-15 01:55:13'),
(63, 'Father\'s Day Cake', 'Price: 500php', '20220615033118.jpg', '2022-06-14 19:31:19', '2022-06-15 01:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'hcabsadmin', '$2y$10$vsnWTv2Bzcxwp0EpBIHLeuWjVdu1mhRS7vX9oA8o2SKlPB/IoCofO', '2022-06-04 06:00:48', '2022-06-04 06:00:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homes`
--
ALTER TABLE `homes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
