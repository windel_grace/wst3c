<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link rel="icon" type="image/png" href="/imgs/logo.png" sizes="16x16">
    
    <title>Hazel's Cakes And Bakes About</title>

    <style>
    body{
      margin: 0;
      padding: 0;
      width: 100%;
      height: 105vh;
      font-family: sans-serif;
      background:url(imgs/melted.png) no-repeat;
      background-size: cover;
      background-position: center;
    }    
    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: #FD90A3;
        border-bottom: 1px solid #fff;
    }
    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }
    nav ul li a{
        line-height: 80px;
        color:black;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;        
    }
    nav ul li a:hover{
        background: pink;
        border-radius: 6px;
    }
    nav .current{
        background-color: pink;
    }
    .zel img{
        top: 15%;
        left: 11%;
        transform: translateX(-50%);
        position: absolute;
        width: 220px;
        height: 200px;
    }
    .about{
        text-align: center;
        font-size: 35px;
        position: absolute;
        top: 50%;
        margin-left: 35px;
        margin-right: 35px;
        font-family: cursive;
    }
    .logo img{
        text-align: center;
        font-size: 35px;
        position: absolute;
        top: 18%;
        left: 80%;
    }
    .text{
        position: absolute;
        left: 40%;
        top: 15%;
        font-size: 30px;
        color:  #ECBADB
    }
    .footer {
    position:fixed;
    bottom:0;
    left:0;
    width:100%;
    height: 70px;
    background: #FD90A3;
    text-align: center;
}
    .text h1{
        margin-top: 90px;
        color: #FD90A3;
    }
    </style>
</head>

    <body>

        <nav>
            <ul>
                <li><a href="/home">Home</a></li>
                <li><a href="/product">Products</a></li>
                <li><a class="current" href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </nav>

       <div class="zel"><img src="imgs/hazeldp.jpg"></div>
       <div class="logo"><img src="imgs/logo.png"></div>
       <div class="text"><h1>ABOUT ME</h1></div>

       <div class="about">
        @foreach($text as $t)
         {{$t->text}}<br><br>
         @endforeach
         <br><br></div>

         <div class="footer">
            <p>© 2022 All rights reserved.</p>
            <p>Developed by: Windel Grace F. Rodillas</p>
        </div>
    </body>
</html>