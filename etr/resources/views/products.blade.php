<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="/imgs/logo.png" sizes="16x16">
    <title>Hazel's Cakes And Bakes Products</title>

    <style>
    body{
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100vh;
      font-family: sans-serif;
      background:url(imgs/melted.png) no-repeat;
      background-size: cover;
      background-position: center;
    }    
    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: #FD90A3;
        border-bottom: 1px solid #fff;
    }
    .logo{
        padding: 0px 25px;
        height: -10px;
        float: left;
        opacity: 80%;
    }
    .logo img{
        width: 100px;
        height: 90px;
    }
    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }
    nav ul li a{
        line-height: 80px;
        color: black;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;        
    }
    nav ul li a:hover{
        background: pink;
        border-radius: 6px;
    }
    nav .current{
        background-color: pink;
    }
    .text1{
        text-align: center; 
        font-family: sans-serif;
        position: absolute;
        top: 12%;
        left: 50%;
        transform: translateX(-50%);
        font-size: 28px;
    }
    .text2{
        text-align: center; 
        font-family: sans-serif;
        position: absolute;
        top: 23%;
        left: 50%;
        transform: translateX(-50%);
        font-size: 28px;
    }
    .row{
        margin-top: 23%;
    }
    .zoom{

    }
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 150px;
}
    img:hover {
  box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
    .footer {
    position:fixed;
    bottom:0;
    left:0;
    width:100%;
    height: 70px;
    background: #FD90A3;
    text-align: center;
}
    #myBtn {
  display: none;
  position: fixed;
  bottom: 80px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: hotpink;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}

#myBtn:hover {
  background-color: pink;
}
    </style>
</head>

    <body>
        <nav>
            <div class= "logo"> <img src="imgs/logo.png" id="logo"></div>
            <ul>
                <li><a href="/home">Home</a></li>
                <li><a class="current" href="/product">Products</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </nav>
        
        <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
        <div class="text1">
            <h1>Products</h1>
        </div>
        <div class="text2">
            <p>Here, you can see different kinds of cakes for any occasions.</p>
        </div>

        <div class="container">
        <div class="row">
        @foreach  ($image as $p)
        <div class="col-md-3 align-items-stretch">
        <div class="item mb-5">
            <a target="_blank" href="{{ URL ('/image/'.$p->image)}}">
            <div class= "images">
        <img class = "card-img-top" style="height:370px; width: 260px; "  src = "{{ URL ('/image/'.$p->image)}}"><a href="https://www.facebook.com/SalazarHazelAnneMendoza" target="_blank">
                        <div class="icon"><i class="fa fa-facebook" style="color:black"></i></div>
        </div></a>
        <h4 style="color: #e75480; font-weight: bold; margin-top:5px; margin-left: 45px;">{{$p->name}}</h4>
        <h4 style="color: #e75480; margin-top:5px; margin-left: 45px;">{{$p->detail}} </h4></a>
        </div>
    </div>
@endforeach
            
            <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
            <script>
            //Get the button
            var mybutton = document.getElementById("myBtn");

            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
  function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

</div>
</div>
<br><br>
<div class="footer">
    <p>© 2022 All rights reserved.</p>
    <p>Developed by: Windel Grace F. Rodillas</p>
        </div>
         
    </body>
</html>