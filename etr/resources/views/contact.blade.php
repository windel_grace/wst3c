<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="/imgs/logo.png" sizes="16x16">
    <title>Hazel's Cakes And Bakes Contacts</title>
    
    <style>
     @import "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
    body{
      margin: 0;
      padding: 0;
      width: 100%;
      height: 130vh;
      font-family: sans-serif;
      background:url(imgs/melted.png) no-repeat;
      background-size: cover;
      background-position: center;
    }    
    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: #FD90A3;
        border-bottom: 1px solid #fff;
    }
    .logo{
        padding: 0px 25px;
        height: -10px;
        float: left;
        opacity: 80%;
    }
    .logo img{
        width: 100px;
        height: 90px;
    }
    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }
    nav ul li a{
        line-height: 80px;
        color: black;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;        
    }
    nav ul li a:hover{
        background:pink;
        border-radius: 6px;
    }
    nav .current{
        background-color: pink;
    }
    .contact{
        .position: relative;
        min-height: 100vh;
        padding: 50px 100px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .contact .content{
        max-width: 800px;
        text-align: center;
        margin-top: 3%;
    }
    .contact .content h2{
        font-size: 45px;
        font-weight: 500;
        margin-top: 25px;
    }
    .contact .content p{
        font-weight: 300;
        font-size: 25px;
    }
    .container{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 5px;
    }
    .container .contactInfo{
        width: 50%;
        display: flex;
        flex-direction: column;
        margin-left: 6%;
    }
    .container .contactInfo .box{
        position: relative;
        padding: 20px 0;
        display: flex;
    }
    .container .contactInfo .box .icon{
        min-width: 60px;
        height: 60px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        font-size: 20px;
    }
    .container .contactInfo .box .text{
        display: flex;
        margin-left: 20px;
        font-size: 20px;
        flex-direction: column;
        font-weight: 300;
    }
     .container .contactInfo .box .text h3{
        font-weight: 500;
        color: pink;
     }
     .leftside{
        margin-top: 50%;
        width: 20%;
        background: pink;
     }
      .content{
        font-weight: 300;
        max-width: 800px;
        text-align: center;
      }
      .mapouter{
        position:relative;
        text-align:right;
        height:350px;
        width:500px;
        margin-right: 50%;
        margin-top: -5%;
    }
    .gmap_canvas {
        overflow:hidden;
        background:none!important;
        height:300px;
        width:1000px;
    }
     .footer {
    position:fixed;
    bottom:0;
    left:0;
    width:100%;
    height: 70px;
    background: #FD90A3;
    text-align: center;
}
   .message{
    margin-bottom: 20px;
   }


    </style>
</head>

    <body>
        <nav>
        <div class= "logo"> <img src="imgs/logo.png" id="logo"></div>
        
            <ul>
                <li><a href="/home">Home</a></li>
                <li><a href="/product">Products</a></li>
                <li><a href="/about">About</a></li>
                <li><a class="current" href="/contact">Contact</a></li>
            </ul>
        </nav>

        <section class="contact">
            <div class="content">
                <h2>Contact Us</h2>
                <p>Get in touch with us!</p>
            </div>

            <div class="container">
                <div class="contactInfo">
                    <div class="box">
                        <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>

                            <div class="text">
                                <h3>Address</h3>
                                 @foreach($address as $add)
                                    {{$add->address}}
                                    @endforeach
                            <br></div>
                        </div>

                        <div class="box">
                        <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <div class="text">
                                <h3>Phone</h3>
                                @foreach($address as $add)
                                    {{$add->phone}}
                                    @endforeach
                           <br> </div>
                        </div>

                         <div class="box">
                        <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <div class="text">
                                <h3>Email</h3>
                                @foreach($address as $add)
                                    {{$add->email}}
                                    @endforeach
                           <br> </div></div>

                        <div class="box">
                        <a href="https://www.facebook.com/SalazarHazelAnneMendoza" target="_blank">
                        <div class="icon"><i class="fa fa-facebook" style="color:black"></i></div></a>
                        <div class="text">
                        <h3>Facebook</h3> </div>
                        </div>

                         <div class="box">
                        <a href="https://instagram.com/hazels.cakes.and.bakes?igshid=YmMyMTA2M2Y=" target="_blank">
                        <div class="icon"><i class="fa fa-instagram" style="color:black;"></i></div></a>
                        <div class="text">
                        <h3>Instagram</h3></div>
                        </div>
                </div>



    <div class="mapouter">
        <div class="gmap_canvas">
            <iframe width="800" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=San%20Hilario%20St.%20Puelay%20Villasis%20Pangasinan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
            </iframe><a href="https://fmovies-online.net"></a><br>
            <a href="https://www.embedgooglemap.net"></a>
        </div>
    </div>
    <br><br>

<br><br>
<div class="footer">
    <p>© 2022 All rights reserved.</p>
    <p>Developed by: Windel Grace F. Rodillas</p>
        </div>
    </body>

</html>