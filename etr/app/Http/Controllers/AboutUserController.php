<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class AboutUserController extends Controller
{
    public function show(){
    $text = DB::select('select * from abouts');
    return view('about')->with('text', $text);
}
}
