<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function validate_form(Request $request)
    {

        $this->validate($request,[
            'username' => 'required',
            'password' => 'required|min:5|max:12',
        ]);

        $userInput = array(
            'username' => $request->get('username'),
            'password' => $request->get('password'),
        );

        if(Auth::attempt($userInput)){
            return redirect('admin/homes');
        }
        else{
            return back()->with('error', 'Wrong username or password');
        }

    }

        public function logout(){
            Auth::logout();
            return redirect('admin/login');
        }
    
}
