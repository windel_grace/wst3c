<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;

	class OrderController extends Controller
	{
		public function iDisplay($itemno, $name, $price){
			return view('item') ->with ('itemno',$itemno) ->with ('name',$name) ->with ('price',$price);
		}

		public function cDisplay($custno,$name,$address){
			return view('customer') ->with ('custno', $custno) ->with ('name', $name) ->with ('address', $address);
		}

		public function oDisplay($custno, $name, $orderno, $date){
			return view('order') ->with ('custno', $custno) ->with ('name', $name) ->with ('orderno', $orderno) ->with ('date', $date);
		}

		public function odDisplay($transactno, $orderno, $itemid, $name, $price, $quantity){
			return view('orderdetail') ->with ('transactno', $transactno) ->with ('orderno', $orderno) ->with ('itemid', $itemid) ->with ('name', $name) ->with ('price', $price) ->with ('quantity', $quantity) ;
		}
	}