<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/item/{itemno}/{name}/{price}', 'App\Http\Controllers\OrderController@iDisplay'); 
 

Route::get('/customer/{custno}/{name}/{address}', 'App\Http\Controllers\OrderController@cDisplay'); 
 

Route::get('/order/{custno}/{name}/{orderno}/{date}', 'App\Http\Controllers\OrderController@oDisplay'); 
 

Route::get('/orderdetail/{transactno}/{orderno}/{itemid}/{name}/{price}/{quantity}', 
    'App\Http\Controllers\OrderController@odDisplay');
             

            