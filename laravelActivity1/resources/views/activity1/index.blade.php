<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href="{{ asset('/public/css/app.css') }}" rel="stylesheet">
	<title>Portfolio</title>
	<style>
	*{
	padding: 0;
	margin: 0;
	font-family: sans-serif;
	box-sizing: border-box;
}
	html{
	scroll-behavior: smooth;
}
	/*Navigation bar*/
	.navbar{
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: #B5AA99;
	position: sticky;
	top: 0;
}
.navbar ul{
	display: flex;
	list-style: none;
	margin: 20px 0px;
}
.navbar ul li{
	font-family: century;
	font-size: 1.1rem;
	font-weight: bold;
}
.navbar ul li a{
	text-decoration: none;
	color: white;
	padding: 8px 25px;
	transition: all .5s ease;
}
.navbar ul li a:hover{
	background-color: white;
	color: black;
	box-shadow: 0 0 10px #008169;
}
/*Home*/
#home{
	display: flex;
	flex-direction: column;
	background-color: rgba(0, 0, 0, 0.5);
	height: 840px;
	justify-content: center;
	align-items: center;
	color: white;
}
#home::before{
	content: "";
	position: absolute;
	top: 0;
	right: 100px;
	background: url('/images/del.jpg') no-repeat center center/cover;
	height: 900px;
	width: 100px;
	z-index: -1;
	opacity: .8;

}
.main{
	display: flex;
	flex-direction: column;
	/*border: 1px solid white;*/
	align-items: center;
	position: absolute;
	top: 30%;
	right: 10%;
}
.headings{
	font-family: century;
	font-size: 3rem;
	text-align: center;
	margin: 40px 0px;
}
.btn{
	padding: 10px 35px;
	background: transparent;
	border: 1px solid white;
	color: white;
	outline: none;
	transition: .6s ease;
}
.btn:hover{
	cursor: pointer;
	background-color: white;
	color: black;
	box-shadow: 0 0 5px white, 0 0 10px white, 0 0 15px white;
	font-weight: bold;
}
/*About*/
#about{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	padding: 20px;
	margin-bottom: 50px;
}
#pic{
	display: flex;
}
#pic img{
	width: 575px;
	height: 400px;
}
#intro{
	display: flex;
	flex-direction: column;
	text-align: justify;
	padding: 10px;
}
#intro h2{
	font-size: 2rem;
	margin-bottom: 20px;
}
/*Contact*/
#contact{
	display: flex;
	flex-direction: column;
	background-color: #4E4637;
	color: white;
	align-items: center;
	padding: 20px;
}
.gallery{
	display: flex;
	flex-wrap: wrap;
	justify-content: space-around;
	box-sizing: border-box;
}
.gallery img{
	width: 360px;
	height: 240px;
	margin: 10px;
}
#contact1{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	background-color: #C2A05E;
	color: white;
	padding: 20px;
	align-items: center;
}
.form{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	align-items: center;
	margin: 20px 0px;

}
.input{
	padding: 12px;
	margin: 15px;
	width: 30%;
	border: none;
	outline: none;
}
.media{
	flex-direction: column;
}

#msg{
	width: 20%;
	padding: 10px;
	margin: 15px;
	border: none;
	outline: none;
}
#send{
	padding: 10px;
	width: 10%;
	margin: 40px ;
	border: none;
	outline: none;
}
#send:hover{
	cursor: pointer;
	box-shadow: 0 0 10px #008169;
}
/*LogIn*/
#login{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	background-color: #008169;
	color: white;
	padding: 20px;
	align-items: center;
}
.form1{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	align-items: center;
	margin: 20px 0px;
}
.input1{
	padding: 12px;
	margin: 15px;
	width: 150%;
	border: none;
	outline: none;
}
#send1{
	padding: 10px;
	width: 60%;
	margin: 40px ;
	border: none;
	outline: none;
}
#send1:hover{
	cursor: pointer;
	box-shadow: 0 0 10px #008169;
}
/*Registration*/
#registration{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	background-color: #4E4637;
	color: white;
	padding: 20px;
	align-items: center;
}
.form2{
	display: flex;
	flex-direction: column;
	box-sizing: border-box;
	align-items: center;
	margin: 20px 0px;
}
.input2{
	padding: 12px;
	margin: 15px;
	width: 150%;
	border: none;
	outline: none;
}
#send2{
	padding: 10px;
	width: 60%;
	margin: 40px ;
	border: none;
	outline: none;
}
#send2:hover{
	cursor: pointer;
	box-shadow: 0 0 10px #008169;
}




	</style>
</head>
<body>

		<nav class="navbar">
			<ul>
				<li><a href="/activity1/index">Home</a></li>
				<li><a href="#about">About Me</a></li>
				<li><a href="#contact">Gallery/Contact Us</a></li>
				<li><a href="#login">Log in Form</a></li>
				<li><a href="#registration">Registration</a></li>
			</ul>
		</nav>
		
		<section id="home">
			<div class="main">
				<h1 class="headings">I AM <br> WINDEL GRACE RODILLAS</h1>
				<button class="btn">
					Log in
				</button>
			</div>
		</section>

		<section id="about">
				<h1 class="headings">ABOUT ME</h1>
				<div id="pic">
				<img src="/images/about.jpg" alt="">
				<img src="/images/resume.png" alt="">
				<div id = "intro">
					<h2>WINDEL GRACE RODILLAS</h2>
					<p>Hello, Im Windel Grace F. Rodillas 3rd Year IT student of Pangasinan
					State University Urdaneta Campus few more steps before we face the real world. I am grateful that I have been a student at Pangasinan State University since were in grade 11 or senior high school. Being a student at Pangsinan State University is not an easy; it will require you to have sacrifice and efforts. My hobbies are playing Volleyball, playing online games, eating, sleeping and I also love reading in webtoon I'm currently reading now the eleceed its about powers and it is comedy.</p>
				</div>
			</div>
		</section>

		<section id="contact">
			<h1 class="headings">Gallery</h1>
			<div class="gallery">
				<img src="/images/1.png" alt="">
				<img src="/images/flower.jpg" alt=""> 
				<img src="/images/3.jpg" alt="">
				<img src="/images/4.jpg" alt="">
				<img src="/images/5.png" alt="">
				<img src="/images/curl.jpg" alt="">
			</div>
		</section>

		<section id="contact1">
			<h1 class="headings">Contact Us</h1
			  <form action="" class="form">
					<input type="text" name="name" class="input" placeholder="Enter Your Name">
					<input type="email" name="email" class="input" placeholder="Enter Your Email">
					<textarea name="msg" id="msg" cols="30" rows="10" placeholder="Enter Your Message"></textarea>
					<input type="submit" value="SEND" id="send">
				</form>
				
				<div class="media">
				<img src="/images/instagram.png" alt=""  style="width: 53px;">
				<img src="/images/facebook.png" alt="" style="width: 53px;">
				<img src="/images/twitter.png" alt="" style="width: 53px;">
				</div>
			</section>

			<section id="login">
				<h1 class="headings">Log In Form</h1>
				<form action="" class="form1">
				<input type="email" name="email" class="input1" placeholder="Enter Your Username">
				<input type="pass" name="pass" class="input1" placeholder="Enter Your Password">
				<input type="submit" value="Log in" id="send1">
				</form>	
			</section>

			<section id="registration">
				<h1 class="headings">Registration</h1>
				<form action="" class="form2">
				<input type="email" name="email" class="input2" placeholder="Enter Your Email">
				<input type="email" name="email" class="input2" placeholder="Enter Username">
				<input type="pass" name="pass" class="input2" placeholder="Enter Password">
				<input type="submit" value="Register" id="send2">
				</form>	
			</section>

</body>
</html>