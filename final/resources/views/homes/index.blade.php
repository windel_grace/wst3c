@extends('homes.layout')
@section('content')


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Welcome Admin! </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('homes.create') }}"> Create Home Content</a>
            </div>
        </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     
     
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Text</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($homes as $home)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $home->text }}</td>
            <td>
                <form action="{{ route('homes.destroy',$home->id) }}" method="POST">
     
                    <a class="btn btn-info" href="{{ route('homes.show',$home->id) }}">Show</a>
      
                    <a class="btn btn-primary" href="{{ route('homes.edit',$home->id) }}">Edit</a>
     
                    @csrf
                    @method('DELETE')
        
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $homes->links() !!}


        
@endsection