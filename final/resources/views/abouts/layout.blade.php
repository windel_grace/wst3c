<!DOCTYPE html>
<html>
<head>
    <title>Admin About</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link rel="icon" type="image/png" href="/imgs/logo.png" sizes="16x16">

    <style>
        <style>
    body{
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100vh;
      font-family: sans-serif;
      background-size: cover;
      background-position: center;
    }    
    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: #FD90A3;
        border-bottom: 1px solid #fff;
    }
    nav .current{
        background-color: pink;
    }
    .logo{
        padding: 0px 25px;
        height: -10px;
        float: left;
        opacity: 25%;
    }
    .logo img{
        width: 100px;
        height: 90px;
    }
    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }
    nav ul li a{
        line-height: 80px;
        color: #fff;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;        
    }
    nav ul li a:hover{
        background: #ECC4BA;
        border-radius: 6px;
    }
    .table{
        margin-top: 55px;
    }
    h2{
        color: black;
        margin-top: 100px;
        margin-left: 20px;
    }
    .pull-right{
        margin-top: 120px;
        background-color: #FD90A3;
    }
    .pull-right a{
        color: white;
    }
    
    .table th{
        background-color: #FD90A3;
        color: white;
    }
    .btn{
        background-color: #FD90A3;
    }
    </style>

</head>

<body>
  
<div class="container">
    @yield('content')</div>
    <nav>
            <div class= "logo"> <img src="/imgs/logo.png" id="logo"></div>
            <ul>
                <li><a href="/admin/homes">Home</a></li>
                <li><a href="/admin/products">Products</a></li>
                <li><a class="current" href="/admin/abouts">About</a></li>
                <li><a href="/admin/contacts">Contact</a></li>
                <li><a href="/admin/login">Logout</a></li>
            </ul>
        </nav>
</div>
   
</body>
</html>