<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
  
    <style>
    @import "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: rgba(0,0,0,0);
        border-bottom: 1px solid #fff;
    }
    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }
    nav ul li a{
        line-height: 80px;
        color: #fff;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;        
    }
    nav ul li a:hover{
        background: #ECC4BA;
        border-radius: 6px;
    }
    body{
      margin: 0;
      padding: 0;
      font-family: sans-serif;
      background:url(/imgs/melted.png) no-repeat;
      background-size: cover;
    }

    .loginbox{
      width: 280px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      color: #ECC4BA;
    }

    .loginbox h1{
      float: left;
      font-size: 40px;
      margin-bottom: 50px;
      padding: 13 0;
    }

    .textbox{
      width: 100%;
      overflow: hidden;
      font-size: 20px;
      padding: 8px 0;
      margin: 8px 0;
      border-bottom: 1px solid pink;
    }
    .textbox i{
      width: 26px;
      float: left;
      height: 35px;
      text-align: center;
    }
    .textbox input{
      border: none;
      outline: none;
      background: none;
      font-size: 25px;
      width: 80%;
      float: left;
      margin: 0 10px;
    }

    .btn{
      width: 100%;
      background: none;
      border: 2px solid #ECBADB;
      padding: 5px;
      font-size: 18 px;
      cursor: pointer;
      margin: 12px 0;
    }
    .logo{
      margin-top: 15px;
      margin-left: 60px;
    }
    </style>
</head>
<body>

    <div class="loginbox">
      <h1>User Login</h1>
    <form action="{{ route('auth.check') }}" method="post">
      @if(Session::get('error'))
          <div class="alert alert-danger">     
            {{ Session::get('error') }}
          </div>
      @endif

      @csrf
    <div class="textbox">
      <i class="fa fa-user" style="font-size: 30px;" aria-hidden="true"></i>
      <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old ('username') }}">
      <span class="text-danger">@error('username'){{ $message }} @enderror</span>
    </div>

    <div class="textbox">
      <i class="fa fa-lock" style="font-size: 30px;" aria-hidden="true"></i>
      <input type="password" class="form-control" placeholder="Password" name="password">
      <span class="text-danger">@error('password'){{ $message }} @enderror</span>
    </div>
    <button type="submit" class="btn btn-block btn-primary" >LOGIN</button>
  </form>
    </div>
   
</body>
</html>
