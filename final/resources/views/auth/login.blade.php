<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>

    
    <title>Login</title>
    <style>
    @import "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";

    .loginbox{
      width: 280px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      color: #ECC4BA;
    }

    .loginbox h1{
      float: left;
      font-size: 40px;
      margin-bottom: 50px;
      padding: 13 0;
    }

    .textbox{
      width: 100%;
      overflow: hidden;
      font-size: 20px;
      padding: 8px 0;
      margin: 8px 0;
      border-bottom: 1px solid pink;
    }
    .textbox i{
      width: 26px;
      float: left;
      height: 35px;
      text-align: center;
    }
    .textbox input{
      border: none;
      outline: none;
      background: none;
      font-size: 25px;
      width: 80%;
      float: left;
      margin: 0 10px;
    }

    .btn{
      width: 100%;
      background: none;
      border: 2px solid #ECBADB;
      padding: 5px;
      font-size: 18 px;
      cursor: pointer;
      margin: 12px 0;
    }
    .logo{
      margin-top: 15px;
      margin-left: 60px;
    }
    </style>
</head>
<body>

    <div class="loginbox">
      <h1>Login</h1>
    <form action="{{ route('auth.check') }}" method="post">
      @if(Session::get('error'))
          <div class="alert alert-danger">     
            {{ Session::get('error') }}
          </div>
      @endif

      @csrf
   <div class="textbox">
      <i class="fa fa-user" aria-hidden="true"></i>
      <input type="text" class="form-control" placeholder="Username" name="username" value="">
      <span class="text-danger">@error('username'){{ $message }}@enderror</span>
    </div>

     <div class="textbox">
      <i class="fa fa-envelope" aria-hidden="true"></i>
      <input type="text" class="form-control" placeholder="Email" name="email" value="">
      <span class="text-danger">@error('username'){{ $message }}@enderror</span>
    </div>

    <div class="textbox">
      <i class="fa fa-lock" aria-hidden="true"></i>
      <input type="password" class="form-control" placeholder="Password" name="password">
      <span class="text-danger">@error('password'){{ $message }} @enderror</span>
    </div>
    <button type="submit" class="btn btn-block btn-primary">Log in</button>
    <br><br>
    <a href="{{ route('register') }}">I don't have an account, Register here!</a>
  </form>
    </div>
   
   
</body>
</html>
