<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <link rel="icon" type="image/png" href="/imgs/logo.png" sizes="16x16">
    
    <title>Hazel's Cakes And Bakes Home</title>

    <style>
    body{
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100vh;
      font-family: sans-serif;
      background:url(imgs/home.webp) no-repeat;
      background-size: cover;
      background-position: center;
    }  

    nav{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100px;
        padding: 10px 90px;
        box-sizing: border-box;
        background: rgba(0,0,0,0);
        border-bottom: 1px solid #ECBADB;
    }

    .logo{
        padding: 0px 25px;
        float: left;
        opacity: 80%;
    }

    .logo img{
        width: 100px;
        height: 90px;
    }

    nav ul{
        list-style: none;
        float: right;
        margin: 0;
        padding: 0;
        display: flex;
    }

    nav ul li a{
        line-height: 10%;
        color: black;
        padding: 12px 30px;
        text-decoration: none;
        font-size: 20px;
        font-weight: bold;
        text-transform: uppercase;        
    }

    nav ul li a:hover{
        background: pink;
        border-radius: 6px;
    }
    nav .current{
        background-color: pink;
    }

    .htext1{
        margin: 0;
        padding: 0;
        text-align: center;
        font-family: sans-serif;
        position: absolute;
        top: 35%;
        left: 50%;
        transform: translateX(-50%);
        font-size: 80px;
        font-weight: bold;

    }

    .htext2{
        margin: 0;
        padding: 0;
        text-align: center;
        font-family: sans-serif;
        position: absolute;
        top: 55%;
        left: 50%;
        transform: translateX(-50%);
        font-size: 35px;
        color: #ECBADB;
    }
    
    .btn{
        background-color: white;
        border: D2BAEC;
        color: black;
        padding: 10px;
        text-decoration: none;
        font-size: 16px;
        border-radius: 12px;
        position: absolute;
        top: 80%;
        left: 29%;
        width: 40%;
    }
    .text{
        font-size: 20px;
        margin-right: 58%;
        font-weight: bold;
        color: black;
    }

    </style>
</head>
    <body>
        <nav>
            <div class= "logo"> <img src="imgs/logo.png" id="logo">
                </div><div class="text"><h3>Hazel's Cakes and Bakes</h3></div>
            <ul>
                <li><a class="current" href="/home">Home</a></li>
                <li><a href="/product">Products</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </nav>
        
        <a class="btn" href="/product">
         <input class="btn" type="button" name="" value="Products">
    </a>

         <div class="htext1">
         @foreach($text as $t)
         {{$t->text}}
         </div>
         <div class="htext2">
         <br><br>
         @endforeach
</div>
    </body>
</html>