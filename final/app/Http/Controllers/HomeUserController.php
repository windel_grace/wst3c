<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeUserController extends Controller
{
    public function show(){
    $text = DB::select('select * from homes');
    return view('home')->with('text', $text);
}
}
