<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class ContactUserController extends Controller
{
    public function show(){
    $address = DB::select('select * from contacts');
    return view('contact')->with('address', $address);
    }
    
    
}