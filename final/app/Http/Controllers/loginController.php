<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function validate_form(Request $request)
    {

        $this->validate($request,[
            'username' => 'required',
            'password' => 'required|min:5|max:12',
        ]);

        $userInput = array(
            'username' => $request->get('username'),
            'password' => $request->get('password'),
        );

        if(Auth::attempt($userInput)){
            return redirect('admin/homes');
        }
        else{
            return back()->with('error', 'Wrong username or password');
        }

    }

     function register(){
        return view ('register');
    }

    function save(Request $request){
        
        //validation
        $request->validate([
            'name'=>'required',
            'username'=>'required',
            'email'=>'required|email|unique:admins',
            'password'=>'required|min:5|max:12'
        ]);

        //inserting data into db. Model way

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $save = $admin->save();

        if ($save){
            return back()->with('success','New user has been successfuly added');
        }else{
            return back()->with('fail','Something went wrong, try again later');
        }
    }

        public function logout(){
            Auth::logout();
            return redirect('admin/login');
        }
    
}
