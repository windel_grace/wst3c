<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\loginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HomeUserController;
use App\Http\Controllers\ProductsUserController;
use App\Http\Controllers\AboutUserController;
use App\Http\Controllers\ContactUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/userlogin', function (){
    return view('userlogin');
});

Route::get('/login', [loginController::class,'index']);
Route::get('/register', [loginController::class, 'register'])->name('register');
Route::post('save', [LoginController::class, 'save'])->name('save');


//Route::get('/userlogin', [loginController::class,'index']);
Route::post('/admin/check', [loginController::class, 'validate_form'])->name('auth.check');
Route::get('/admin/logout', [loginController::class,'logout']);

//admin
Route::resource('/admin/products', ProductController::class);
Route::resource('/admin/contacts', ContactController::class);
Route::resource('/admin/abouts', AboutController::class);
Route::resource('/admin/homes', HomeController::class);

//user
Route::get('/home', [HomeUserController::class, 'show']);
Route::get('/product', [ProductsUserController::class, 'show']);
Route::get('/about', [AboutUserController::class, 'show']);
Route::get('/contact', [ContactUserController::class, 'show']);

