<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('quiz1');
});

Route::get('/item/{itemnom}/{name}/{price}', 
    function ($itemnom,$name,$price) {
    return "Item No: ".$itemnom." Name: ".$name." Price: ".$price;
});

Route::get('/customer/{custnum}/{name}/{address}/{age?}', 
    function ($custnum,$name,$address,$age="Undefined") {
    return "Customer No: ".$custnum." Name: ".$name." Address: ".$address." Age: ".$age;
});

Route::get('/order/{custid}/{name}/{orderno}/{date}', 
    function ($custid,$name,$orderno,$date) {
    return "Customer ID: ".$custid." Name: ".$name." Order No: ".$orderno." Date: ".$date;
});

Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{quantity}', 
    function ($transno,$orderno,$itemid,$name,$price,$quantity) {
    return "Trans No: ".$transno." Order No: ".$orderno." Item ID: ".$itemid." Name: ".$name." Price: ".$price." Quantity: ".$price*$quantity;
});