<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error</title>
    <style>
        .error
        {
            background-color: #FF6347;
            text-align: center;
            color: white;
            padding: 20px;
        }
    </style>
</head>
<body>
    <div class="error">
            <h1>404 Error</h1>
            <p>Error Finding Webpage</p>
    </div>

</body>
</html>